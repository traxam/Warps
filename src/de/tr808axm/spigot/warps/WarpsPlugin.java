/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.warps;

import de.tr808axm.spigot.pluginutils.ChatUtil;
import de.tr808axm.spigot.pluginutils.ProxiedCommandExecutor;
import de.tr808axm.spigot.warps.command.WarpsCommandExecutor;
import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;

/**
 * Warps-plugin's main class.
 * Created by tr808axm on 18.09.2016.
 */
public class WarpsPlugin extends JavaPlugin {
    private ChatUtil chatUtil;
    private WarpManager warpManager;

    @Override
    public void onLoad() {
        super.onLoad();
        try {
            warpManager = new WarpManager(this);
        } catch (IOException | InvalidConfigurationException e) {
            getLogger().severe("Error at loading/creating warps.yml: " + e.getClass().getSimpleName());
            getServer().getPluginManager().disablePlugin(this);
        }
    }

    @Override
    public void onEnable() {
        super.onEnable();
        chatUtil = new ChatUtil(getDescription(), ChatColor.GOLD, getServer());
        registerCommands();
        getLogger().info("Enabled!");
    }

    private void registerCommands() {
        getCommand("warps").setExecutor(new WarpsCommandExecutor(this));
        getCommand("warp").setExecutor(new ProxiedCommandExecutor(getCommand("warps"), "warp"));
        getCommand("spawn").setExecutor(new ProxiedCommandExecutor(getCommand("warps"), "spawn"));
    }

    @Override
    public void onDisable() {
        super.onDisable();
        getLogger().info("Disabled!");
    }

    public ChatUtil getChatUtil() {
        return chatUtil;
    }

    public WarpManager getWarpManager() {
        return warpManager;
    }
}
