/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.warps.command;

import de.tr808axm.spigot.pluginutils.ChatUtil;
import de.tr808axm.spigot.pluginutils.UnlinkedLocation;
import de.tr808axm.spigot.warps.WarpManager;
import de.tr808axm.spigot.warps.WarpsPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Handles the /warps command.
 * Created by tr808axm on 18.09.2016.
 */
public class WarpsCommandExecutor implements CommandExecutor {
    private final WarpsPlugin plugin;
    private final Map<String, String> commandHelp;

    public WarpsCommandExecutor(WarpsPlugin plugin) {
        this.plugin = plugin;
        commandHelp = new HashMap<>();
        commandHelp.put("/warps [help]", "Show the help menu.");
        commandHelp.put("/warps delete <name>", "Delete the warp <name>.");
        commandHelp.put("/warps list", "Lists all warps.");
        commandHelp.put("/warps set <name>", "Sets the warp <name> at your current position.");
        commandHelp.put("/warps spawn", "Teleports you to the spawn.");
        commandHelp.put("/warps warp <name>", "Teleports you to the <name>-Warp.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length >= 1) {
            if (args[0].equalsIgnoreCase("delete") || args[0].equalsIgnoreCase("del") || args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("rem")) {
                if (!checkPermission(sender, "warps.admin.delete")) return true;
                if (args.length >= 2) {
                    String warpName = args[1];
                    if (plugin.getWarpManager().deleteWarp(warpName) != null)
                        plugin.getChatUtil().send(sender, "Deleted warp '" + ChatUtil.VARIABLE + warpName.toLowerCase() + ChatUtil.POSITIVE + "'!", true);
                    else
                        plugin.getChatUtil().send(sender, "There was no warp called '" + ChatUtil.VARIABLE + warpName.toLowerCase() + ChatUtil.NEGATIVE + "'!", false);
                    return true;
                }
                plugin.getChatUtil().send(sender, ChatUtil.buildHelpMenu("/warps delete", "/warps delete <name>", commandHelp.get("/warps delete <name>")), true);
                return true;
            } else if (args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?")) {
                plugin.getChatUtil().send(sender, ChatUtil.buildHelpMenu("Warps", commandHelp), true);
                return true;
            } else if (args[0].equalsIgnoreCase("list") || args[0].equalsIgnoreCase("show")) {
                if (!checkPermission(sender, "warps.user.list")) return true;
                Map<String, UnlinkedLocation> warps = plugin.getWarpManager().getWarps();
                DecimalFormat df = new DecimalFormat("#0.000");
                String[] messages;
                if (warps.size() > 0) {
                    messages = new String[1 + (warps.size() * 3)];
                    messages[0] = "----- Warp-list -----";
                    int i = 1;
                    for (String warpName : warps.keySet()) {
                        messages[i++] = "";
                        messages[i++] = ChatUtil.VARIABLE + warpName;
                        messages[i++] = ChatUtil.DETAILS + ChatUtil.formatLocation(warps.get(warpName), true, false);
                    }
                } else {
                    messages = new String[2];
                    messages[0] = "----- Warp-list -----";
                    messages[1] = ChatUtil.VARIABLE.toString() + ChatColor.ITALIC + "(none)";
                }
                plugin.getChatUtil().send(sender, messages, true);
                return true;
            } else if (args[0].equalsIgnoreCase("set") || args[0].equalsIgnoreCase("create")) {
                if (!checkPermission(sender, "warps.admin.set") || !checkIsPlayer(sender)) return true;
                if (args.length >= 2) {
                    String warpName = args[1];
                    try {
                        plugin.getWarpManager().setWarp(warpName, UnlinkedLocation.fromLocation(((Player) sender).getLocation()));
                        plugin.getChatUtil().send(sender, "Set warp '" + ChatUtil.VARIABLE + warpName + ChatUtil.POSITIVE + "'!", true);
                    } catch (WarpManager.IllegalWarpNameException e) {
                        plugin.getChatUtil().send(sender, "Could not set warp '" + ChatUtil.VARIABLE + warpName + ChatUtil.NEGATIVE + "': " + e.getMessage() + ".", false);
                    }
                    return true;
                }
                plugin.getChatUtil().send(sender, ChatUtil.buildHelpMenu("/warps set", "/warps set <name>", commandHelp.get("/warps set <name>")), true);
                return true;
            } else if (args[0].equalsIgnoreCase("spawn")) {
                if (!checkPermission(sender, "warps.user.warp.spawn") || !checkIsPlayer(sender)) return true;
                try {
                    if (plugin.getWarpManager().warp((Player) sender, "spawn"))
                        plugin.getChatUtil().send(sender, "You were warped to " + ChatUtil.VARIABLE + "the spawn" + ChatUtil.POSITIVE + ".", true);
                    else
                        plugin.getChatUtil().send(sender, "The spawn is not set! (Contact an admin if you believe that this is an error)", false);
                } catch (UnlinkedLocation.LinkedLocationNotAvailableException e) {
                    plugin.getChatUtil().send(sender, "The spawn is currently not available.", false);
                }
                return true;
            } else if (args[0].equalsIgnoreCase("warp") || args[0].equalsIgnoreCase("teleport") || args[0].equalsIgnoreCase("tp")) {
                if (!checkPermission(sender, "warps.user.warp") || !checkIsPlayer(sender)) return true;
                if (args.length >= 2) {
                    String warpName = args[1];
                    try {
                        if (plugin.getWarpManager().warp((Player) sender, warpName))
                            plugin.getChatUtil().send(sender, "You were warped to '" + ChatUtil.VARIABLE + warpName + ChatUtil.POSITIVE + "'.", true);
                        else
                            plugin.getChatUtil().send(sender, "The warp '" + ChatUtil.VARIABLE + warpName + ChatUtil.NEGATIVE + "' does not exist!", false);
                    } catch (UnlinkedLocation.LinkedLocationNotAvailableException e) {
                        plugin.getChatUtil().send(sender, "The warp '" + ChatUtil.VARIABLE + warpName + ChatUtil.NEGATIVE + "' is currently not available.", false);
                    }
                    return true;
                }
                plugin.getChatUtil().send(sender, ChatUtil.buildHelpMenu("/warps warp", "/warps warp <name>", commandHelp.get("/warps warp <name>")), true);
                return true;
            }
            plugin.getChatUtil().send(sender, ChatUtil.INVALID_ARGUMENTS_MESSAGE, false);
            return true;
        }
        plugin.getChatUtil().send(sender, ChatUtil.buildHelpMenu("Warps", commandHelp), true);
        return true;
    }

    private boolean checkPermission(CommandSender sender, String permission) {
        if (sender.hasPermission(permission)) return true;
        else {
            plugin.getChatUtil().send(sender, ChatUtil.NO_PERMISSION_MESSAGE, false);
            return false;
        }
    }

    private boolean checkIsPlayer(CommandSender sender) {
        if (sender instanceof Player) return true;
        else {
            plugin.getChatUtil().send(sender, ChatUtil.NO_EXECUTION_FROM_CONSOLE, false);
            return false;
        }
    }
}
