/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.spigot.warps;

import de.tr808axm.spigot.pluginutils.ConfigurationLoader;
import de.tr808axm.spigot.pluginutils.UnlinkedLocation;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Used to store, save and load warps.
 * Created by tr808axm on 18.09.2016.
 */
public class WarpManager implements ConfigurationLoader.ConfigurationReader, ConfigurationLoader.ConfigurationWriter {
    public static class IllegalWarpNameException extends IllegalArgumentException {
        IllegalWarpNameException(String message) {
            super(message);
        }

        static void validateWarpName(String warpName) throws IllegalWarpNameException {
            if (warpName == null || warpName.equals(""))
                throw new IllegalWarpNameException("warp name may not be empty");
            if (warpName.startsWith(" ") || warpName.endsWith(" "))
                throw new IllegalWarpNameException("warp name may not begin or end with a space (' ')");
            if (warpName.contains(":") || warpName.contains("=") || warpName.contains("."))
                throw new IllegalWarpNameException("warp name may not contain the characters ':', '.' and '='");
        }
    }

    private final WarpsPlugin plugin;
    private final ConfigurationLoader warpsConfigurationLoader;
    private Map<String, UnlinkedLocation> warps;

    // Package-Private method, shall only be called by de.tr808axm.spigot.warps.WarpsPlugin.
    WarpManager(final WarpsPlugin plugin) throws IOException, InvalidConfigurationException {
        this.plugin = plugin;
        warpsConfigurationLoader = new ConfigurationLoader(new File(plugin.getDataFolder(), "warps.yml"), plugin.getLogger(), new ConfigurationLoader.InputStreamProvider() {
            @Override
            public InputStream provideInputStream() {
                return plugin.getResource("warps.yml");
            }
        });
        warpsConfigurationLoader.addConfigurationReader(this);
        warpsConfigurationLoader.addConfigurationWriter(this);
        warpsConfigurationLoader.reload(true);
    }

    @Override
    public void readConfig(FileConfiguration warpsFileConfiguration) {
        plugin.getLogger().info("Reading warps from disk...");
        ConfigurationSection warpsConfiguraitonSection = warpsFileConfiguration.getConfigurationSection("warps");
        if (warpsConfiguraitonSection == null)
            warpsConfiguraitonSection = warpsFileConfiguration.createSection("warps");

        warps = new HashMap<>();
        for (String warpName : warpsConfiguraitonSection.getKeys(false)) {
            UnlinkedLocation warp = UnlinkedLocation.deserialize(warpsConfiguraitonSection.getConfigurationSection(warpName).getValues(false));
            warps.put(warpName, warp);
        }
        plugin.getLogger().info("...found " + warps.size() + " warps.");
    }

    @Override
    public void writeConfig(FileConfiguration warpsFileConfiguration) {
        plugin.getLogger().info("Saving warps to disk...");
        ConfigurationSection warpsConfigurationSection = warpsFileConfiguration.createSection("warps");

        for (String warpName : warps.keySet()) warpsConfigurationSection.set(warpName, warps.get(warpName).serialize());
        plugin.getLogger().info("...successfully wrote data.");
    }

    public UnlinkedLocation getWarp(String warpName) { // public for future API access point.
        return warps.get(warpName.toLowerCase());
    }

    public UnlinkedLocation setWarp(String warpName, UnlinkedLocation warpLocation) {
        IllegalWarpNameException.validateWarpName(warpName);
        if (warpLocation == null)
            throw new IllegalArgumentException("warpLocation may not be null");
        UnlinkedLocation oldWarpLocation = warps.put(warpName.toLowerCase(), warpLocation);
        warpsConfigurationLoader.save(true, true);
        return oldWarpLocation;
    }

    public UnlinkedLocation deleteWarp(String warpName) {
        UnlinkedLocation removedWarpLocation = warps.remove(warpName.toLowerCase());
        if (removedWarpLocation != null) warpsConfigurationLoader.save(true, true);
        return removedWarpLocation;
    }

    public Map<String, UnlinkedLocation> getWarps() {
        return warps;
    }

    /**
     * This method does not belong here. :( Might be (re)moved in the future.
     *
     * @return false if warp is not existant
     * @throws UnlinkedLocation.LinkedLocationNotAvailableException if Location is not available.
     */
    public boolean warp(Player player, String warpName) throws UnlinkedLocation.LinkedLocationNotAvailableException {
        if (player == null) throw new NullPointerException("player may not be null");
        if (warpName == null) throw new NullPointerException("warpName may not be null");
        UnlinkedLocation unlinkedWarpLocation = getWarp(warpName);
        if (unlinkedWarpLocation != null) {
            player.teleport(unlinkedWarpLocation.getLinkedLocation());
            return true;
        }
        return false;
    }
}
